//
import {View, Text} from "react-native";
// On importe la MainStack
import MainStack from "./navigations/MainStack";
// On importe NavigationContainer (il ne peut n'y en avoir qu'un par appli)
import { NavigationContainer } from "@react-navigation/native";
// On importe useFont pour charger les fonts
import { useFonts } from 'expo-font';

export default function App() {
  // On charge les fonts
  let [fontsLoaded] = useFonts({
    "meowScript": require("./assets/fonts/MeowScript-Regular.ttf"),
    "nunito": require("./assets/fonts/Nunito-Regular.ttf"),
    "openSansLight": require("./assets/fonts/OpenSans-Light.ttf"),
    "openSansMedium": require("./assets/fonts/OpenSans-Medium.ttf"),
  });
  // On vérifie si la font est chargée
  if (!fontsLoaded) {
    return(<View style={{ flex:1, backgroundColor: "black", alignItems:"center", justifyContent:"center" }}>
      <Text style={{ color: "white", fontSize: 50 }}>Loading...</Text>
    </View>)
  }
  
  return (
    <NavigationContainer>
      <MainStack/>
    </NavigationContainer>
  );
}
