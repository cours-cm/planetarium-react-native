import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList, SafeAreaView, ImageBackground } from 'react-native';

import bg from '../assets/images/bg-satelite.jpg';
// On importe axios
import axios from "axios";
// On déclare l'url de l'api
const url = "https://toctocapi.com/api/themes/planetarium";
// On déclarer l'url d'accès aux images
const urlImage = "https://toctocapi.com/images-api/";


export default function InsoliteScreen({ navigation }) {
    // On déclare un state
    const [listData, setListData] = React.useState([]);
    const [intro, setIntro] = React.useState("");
    //
    useEffect(() => {
        // On fait une requête async pour récupérer les données de l'api
        axios.get(url)
            .then((response) => {
                setListData(response.data.categories[1].items);
                setIntro(response.data.categories[1].description);
            })
            .catch((error) => {
                console.log(error);
            })
    }, []);
     // On déclare une fonction qui va renvoyer le rendu d'un item de la flatlist
     const renderItem = ( {item} ) =>{
        return(
            <TouchableOpacity style={ styles.touch } onPress={ ()=> navigation.navigate("InsoliteDetail", { item: item, urlMedia: urlImage, headerTitle: item.name } )}>
                <Image style={ styles.image } source={{ uri: urlImage + item.medias[0].filename}} />
            </TouchableOpacity>
        );
    }
    return (
        <ImageBackground source={bg} style={styles.container}>

            <SafeAreaView style={styles.container2}>
                <Text style={styles.intro}>{intro}</Text>
                <FlatList data={ listData } keyExtractor={ item => item['@id'] } renderItem = { renderItem }  style={ styles.flat } numColumns={ 3 }/>
            </SafeAreaView>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        resizeMode: 'contain',
    },
    container2: {
        flex: 1,
    },
    flat:{
        alignSelf: 'center',
    },
    image:{
        width: 100,
        height: 100,
        margin: 10,
    },
    intro: { 
        marginTop: 90,
        color: '#fff',
        fontSize: 20,
        paddingHorizontal: 20,
        textAlign: 'justify',
    }
});