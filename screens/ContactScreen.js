import React from "react";

import { StyleSheet, SafeAreaView, Text, ImageBackground, ScrollView, TextInput, Dimensions, TouchableOpacity } from "react-native";
// On importe la status bar de expo
import { StatusBar } from "expo-status-bar";
// On importe les constants de expo
import Constants from "expo-constants";
// On importe axios pour envoyer des requêtes
import axios from "axios";

import bg from "../assets/images/bg-contact.jpg";

export default function ContactScreen() {
    // On met en place des states pour les valeurs des champs et les messages d'erreur
    const [nom, setNom] = React.useState("");
    const [prenom, setPrenom] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [message, setMessage] = React.useState("");
    const [emailError, setEmailError] = React.useState("");
    const [messageError, setMessageError] = React.useState("");
    // Mise en place de la fonction de vérification et d'envoi du formulaire
    const sendForm = () => {
        // On vérifie que l'email est valide en vérifiant qu'il contient un @ et un .
        // a@a.aa
        if (email.indexOf("@") === -1 || email.indexOf(".") === -1 || email.length < 6) {
            setEmailError("L'email est invalide.");
        } else {
            setEmailError("");
        }
        // On vérifie que le message fait au moins 10 caractères
        if (message.length < 10) {
            setMessageError("Le message doit faire au moins 10 caractères.");
        }else{
            setMessageError("");
        }
        // Si les champs sont valide, on envoie le formulaire
        if (emailError === "" && messageError === "") {
            // On crée un objet FormData pour envoyer les données
            let datas = new FormData();
            // On ajoute les données au FormData
            datas.append("nom", nom);
            datas.append("prenom", prenom);
            datas.append("email", email);
            datas.append("message", message);
            // On envoie les données
            axios.post("https://monsite.fr/api/contact", datas)
            .then((response) => {
                // On réinitialise toutes states
                setNom("");
                setPrenom("");
                setEmail("");
                setMessage("");
            })
            .catch((error) => {
                console.log(error);
            });

        }
    };
    return(
        <ImageBackground source={ bg } style={ styles.container}>
            <SafeAreaView style={ styles.container2 }>
                <ScrollView style={ styles.scroll }>
                    <Text style={ styles.titre }>Contactez nous!</Text>
                    <TextInput placeholder="Nom" style={ styles.input } placeholderColor="#0BC6F4" onChangeText={ (text) => setNom(text) }/>
                    <TextInput placeholder="Prénom" style={[ styles.input, { marginBottom: 0 }] } placeholderColor="#0BC6F4" onChangeText={ (text) => setPrenom(text) }/>
                    
                    <Text style={ styles.error }>{ emailError }</Text>
                    <TextInput placeholder="Email" style={ [styles.input, { marginBottom: 0 }] } placeholderColor="#0BC6F4" onChangeText={ (text) => setEmail(text) }/>
                    
                    <Text style={ styles.error }>{ messageError }</Text>
                    <TextInput placeholder="Entrer votre message" style={[ styles.input, { textAlignVertical: 'top' }] } placeholderColor="#0BC6F4" onChangeText={ (text) => setMessage(text) } numberOfLines={ 6 } multiline={ true } />
                    
                    <TouchableOpacity style={ styles.button } onPress={ sendForm }>
                        <Text style={ styles.buttonText }>Envoyer</Text>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
            <StatusBar style="light" />
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    error:{
        color: "red",
        fontFamily: "openSansLight",
        fontSize: 15,
    },
    button: {
        backgroundColor: "#1A65DA",
        paddingVertical: 5,
        borderRadius: 5,
    },
    buttonText: {
        color: "white",
        fontSize: 20,
        fontFamily: "nunito",
        textAlign: "center",
    },
    container: {
        flex: 1,
        width: "100%",
        resizeMode: "contain",
    }, 
    container2: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    },
    titre:{
        fontFamily: "meowScript",
        fontSize: 50,
        color: "white",
        textAlign: "center",
        marginTop: 80,
    },
    scroll:{
        marginHorizontal: 15,
    },
    input:{
        backgroundColor: "white",
        fontSize: 20,
        fontFamily: "openSansLight",
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5,
        marginBottom: 20,
    }
});