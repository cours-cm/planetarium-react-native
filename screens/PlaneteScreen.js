import React, { useEffect } from "react";
// On importe les composants ReactNative
import { StyleSheet, View, Text, Image, TouchableOpacity, ImageBackground, FlatList, SafeAreaView } from "react-native";
// On importe la statut bar de expo
import { StatusBar } from "expo-status-bar";
// On import les constantes d'expo
import Constants from "expo-constants";
// On importe l'image de fond
import bg from "../assets/images/black-sun.jpg";
// On importe axios
import axios from "axios";
// On déclare l'url de l'api
const url = "https://toctocapi.com/api/themes/planetarium";
// On déclarer l'url d'accès aux images
const urlImage = "https://toctocapi.com/images-api/";


export default function PlaneteScreen({ navigation }) {
    // On déclare un state
    const [listData, setListData] = React.useState([]);
    //
    useEffect(() => {
        // On fait une requête async pour récupérer les données de l'api
        axios.get(url)
        .then((response) => {
            setListData(response.data.categories[0].items);
        })
        .catch((error) => {
            console.log(error);
        })
    }, []);
    // On déclare une fonction qui va renvoyer le rendu d'un item de la flatlist
    const renderItem = ( {item} ) =>{
        return(
            <TouchableOpacity style={ styles.touch } onPress={ ()=> navigation.navigate("PlaneteDetail", { item: item, urlMedia: urlImage, headerTitle: item.name } )}>
                <Image style={ styles.image } source={{ uri: urlImage + item.medias[0].filename}} />
                <Text style={ styles.text }>{ item.name }</Text>
            </TouchableOpacity>
        );
    }
    return(
        <ImageBackground source={ bg } style={ styles.container }>
            <SafeAreaView style={ styles.container2 }>
                <FlatList data={ listData } keyExtractor={ item => item['@id'] } renderItem = { renderItem }  style={ styles.flat }/>
            </SafeAreaView>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    image: {
        width:80,
        height:80,
        borderRadius: 40,
    },
    flat: {
        marginTop: 90,
    },
    touch: {
        backgroundColor: "#0A4179",
        marginHorizontal: 20,
        marginBottom: 20,
        paddingVertical: 6,
        paddingHorizontal: 10,
        borderRadius: 40,
        flexDirection: "row",
        alignItems: "center",
    },
    text: {
        color: "white",
        fontSize: 30,
        fontFamily: "nunito",
        marginLeft: 20,
    },
    container: {
        flex: 1,
        resizeMode: "cover",
    }, 
    container2: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    }
});