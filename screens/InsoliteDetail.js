import React from "react";
import { StyleSheet, SafeAreaView, Text, Image, ScrollView, Dimensions} from "react-native";
import  Constants from "expo-constants";
// On importe autoheight webview pour prendre en charge l'affichage de l'html
import AutoHeightWebView from "react-native-autoheight-webview";

// On récupere la largeur de l'écran
const windowWidth = Dimensions.get('window').width;

export default function PlaneteDetail({ route }) {
    // On récupère les données de la routes (paramètres)
    const { item, urlMedia  } = route.params;
    return(
            <SafeAreaView style={ styles.container }>
                <ScrollView>
                    <Image style={ styles.image1 } source={{ uri: urlMedia + item.medias[0].filename}} />
                    <AutoHeightWebView style={{ width: windowWidth - 20, marginVertical: 35, opacity: 0.99 }} source={{ html: '<div style="color: white; font-size:20px;padding:0 15px;text-align:justify;">'+item.description+'</div>' }} scrollEnabled={ false } androidHardwareAcceleratorDisabled={ true }/>
                </ScrollView>
            </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width: "100%",
        paddingTop: Constants.statusBarHeight,
        backgroundColor: "#000",
    },
    image1: {
        width: windowWidth*0.5,
        height: windowWidth*0.5,
        borderRadius: windowWidth*0.25,
        alignSelf: "center",
    },
});