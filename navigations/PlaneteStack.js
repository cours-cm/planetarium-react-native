// On importe le createStackNavigation pour instancier la pile
import { createStackNavigator } from "@react-navigation/stack";
// On instancie la pile de navigation
const Stack = createStackNavigator();
// On importe les écrans
import PlaneteScreen from "../screens/PlaneteScreen";
import PlaneteDetail from "../screens/PlaneteDetail";

export default function PlaneteStack() {
    return(
        <Stack.Navigator screenOptions={{ gestureEnabled: true }}>
            <Stack.Screen name="PlaneteScreen" component={PlaneteScreen} options={{headerShown: false}}/>
            <Stack.Screen name="PlaneteDetail" component={PlaneteDetail} options={ ({route})=>({
                title: route.params.headerTitle, 
                headerStyle: { backgroundColor: "#000" },
                headerTintColor: "#fff"
                }) }/>
        </Stack.Navigator> 
    );
}