// On importe createBottomTabNavigator qui va nous permettre de créer une barre de navigation
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
// On importe les écrans
import HomeStack from "./HomeStack";
import PlaneteStack from "./PlaneteStack";
import InsoliteStack from "./InsoliteStack";
import ContactScreen from "../screens/ContactScreen";
// On importe les icones
import { Ionicons, FontAwesome, Entypo } from '@expo/vector-icons';
// On instancie la barre de navigation
const Tab = createBottomTabNavigator();

const inactiveTabColor = "grey";
// On déclare les icones utilisés dans la barre de navigation
const activeHomeColor = "#470A91";
const IconHome = ( props ) => (<Ionicons name="ios-home" size={props.size} color={ props.focused ? activeHomeColor : inactiveTabColor} />);
const activePlaneteColor = "#03B5D9";
const IconPlanete = (props) => (<Ionicons name="planet" size={props.size} color={ props.focused ? activePlaneteColor : inactiveTabColor} />);
const activeInsoliteColor = "#F2C94C";
const IconInsolite = (props) => (<FontAwesome name="gift" size={props.size} color={ props.focused ? activeInsoliteColor : inactiveTabColor} />);
const activeContactColor = "#1DC96B";
const IconContact = (props) => (<Entypo name="mail" size={props.size} color={ props.focused ? activeContactColor : inactiveTabColor } />);

export default function BottomTab(){
    return(
        <Tab.Navigator>
            <Tab.Screen name="HomeStack" component={ HomeStack } options={{headerShown: false, tabBarIcon: IconHome, tabBarLabel: "Accueil", tabBarActiveTintColor: activeHomeColor, tabBarInactiveTintColor: inactiveTabColor }}/>
            <Tab.Screen name="PlaneteStack" component={ PlaneteStack } options={{headerShown: false, tabBarIcon: IconPlanete, tabBarLabel: "Planètes", tabBarActiveTintColor: activePlaneteColor, tabBarInactiveTintColor: inactiveTabColor }}/>
            <Tab.Screen name="InsoliteStack" component={ InsoliteStack } options={{headerShown: false, tabBarIcon: IconInsolite, tabBarLabel: "Insolites", tabBarActiveTintColor: activeInsoliteColor, tabBarInactiveTintColor: inactiveTabColor }}/>
            <Tab.Screen name="ContactScreen" component={ ContactScreen } options={{ headerShown: false, tabBarIcon: IconContact, tabBarActiveTintColor: activeContactColor, tabBarInactiveTintColor: inactiveTabColor, tabBarLabel: "Contact" }}/>
        </Tab.Navigator>
    );
}