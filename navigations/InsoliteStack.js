import { createStackNavigator } from "@react-navigation/stack";


const Stack = createStackNavigator();


import InsoliteScreen from '../screens/InsoliteScreen';
import InsoliteDetail from '../screens/InsoliteDetail';

export default function InsoliteStack() {

    return (
        <Stack.Navigator>
            <Stack.Screen name="Insolite" component={InsoliteScreen} options={{ headerShown: false }}/>
            <Stack.Screen name="InsoliteDetail" component={InsoliteDetail} options={ ({route})=>({
                title: route.params.headerTitle, 
                headerStyle: { backgroundColor: "#CD350C" },
                headerTintColor: "#fff"
                }) }/>
        </Stack.Navigator>
    );
}
